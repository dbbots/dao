import logging
import json

from flask import Flask, request
from flask_cors import CORS

from dao import DAO

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

app = Flask(__name__, instance_relative_config=True)
CORS(app)
# app.config.from_object('config')
# app.config.from_pyfile('config.py')
dao = DAO(app.config)

# configObj added for testing
configObj = app.config

def returnJson(row, dict_names):

    dict_deal_json = {}

    for indx, entry in enumerate(row):
        dict_deal_json[dict_names[indx]] = entry

    deal_json = json.dumps(dict_deal_json, default=str)
    print(deal_json)

    return json.loads(deal_json)

@app.route('/chart', methods=['GET'])
def receive_chart():
    deal_list = dao.retrieve_chart_from_db()
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)
    return json.dumps(dict_list), 200

@app.route('/ctpy', methods=['GET'])
def receive_cpty():
    deal_list = dao.retrieve_ctpy_from_db()
    dao.db.commit()

    dict_names = ['ctpy', 'ctpy_count']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)
    return json.dumps(dict_list), 201

@app.route('/dateperiod/<start_d>/<end_d>', methods=['GET'])
def receive_deal_over_date_period(start_d = None, end_d = None):
    print(type, start_d, end_d)
    deal_list = dao.retrieve_deal_over_date_period(start_d, end_d)
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)

    return json.dumps(dict_list), 202

@app.route('/datetonow/<start_d>', methods=['GET'])
def receive_deal_to_yesterday(start_d = None):
    deal_list = dao.retrieve_deal_over_date_period(start_d)
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)

    return json.dumps(dict_list), 203

@app.route('/timeperiodatdate/<start_t>/<end_t>/<date_choice>', methods=['GET'])
def receive_deal_over_time_period(start_t = None, end_t = None, date_choice = None):
    print(start_t, end_t, date_choice)
    deal_list = dao.retrieve_deal_over_time_period(start_t, end_t, date_choice)
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)

    print(dict_list)

    return json.dumps(dict_list), 205

@app.route('/timeperiodyesterday/<start_t>/<end_t>', methods=['GET'])
def receive_deal_over_time_period_yesterday(start_t = None, end_t = None):
    deal_list = dao.retrieve_deal_over_time_period(start_t, end_t)
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)

    print(dict_list)

    return json.dumps(dict_list), 206

@app.route('/fromtimeyesterday/<start_t>', methods=['GET'])
def receive_deal_over_time_period_yesterday_until_now(start_t = None):
    deal_list = dao.retrieve_deal_over_time_period(start_t)
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)

    print(dict_list)

    return json.dumps(dict_list), 207

@app.route('/alltimeatdate/<date_choice>', methods=['GET'])
def receive_deal_on_date(date_choice = None):
    deal_list = dao.retrieve_deal_at_date_over_time_period(date_choice)
    dao.db.commit()

    dict_names = ['instrumentName','price', 'type', 'date', 'time']

    dict_list = []

    for row in deal_list:
        json_dict = returnJson(row, dict_names)
        dict_list.append(json_dict)

    print(dict_list)

    return json.dumps(dict_list), 208


@app.route('/dealdata', methods=['POST'])
def post_json_db():
    instrument_deal = request.get_json()
    print(instrument_deal)

    instrument_deal = json.loads(instrument_deal)

    dao.write_into_db(instrument_deal)
    dao.retrieve_chart_from_db()
    dao.db.commit()
    dao.retrieve_instrument_from_db()

    return 'JSON object received', 209

@app.route('/endpoint', methods=['GET'])
def get_endpoints():
    instrument_list = dao.end_positions_query()
    dao.db.commit()

    instruments = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
               "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"]
    cleaned_instrument_list = []
    for x in instrument_list:
        cleaned_instrument_list.append(x[0][0])

    instrument_counts = []
    print(cleaned_instrument_list)

    for x in range(len(instruments)):
        tempDict = {}
        tempDict["instrumentName"] = instruments[x]
        tempDict["endPosition"] = round(float(cleaned_instrument_list[x]),2)
        instrument_counts.append(tempDict)

    return json.dumps(instrument_counts), 210

@app.route('/profitloss', methods=['GET'])
def get_profitloss():
    profitloss = dao.profit_loss_query()
    dao.db.commit()

    instruments = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                   "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"]

    dict_profitloss = []
    for x in range(len(instruments)):
        tempDict = {}
        tempDict["instrumentName"]=instruments[x]
        tempDict["profitLoss"] = profitloss[x+1]
        dict_profitloss.append(tempDict)

    return json.dumps(dict_profitloss), 211

@app.route('/profitloss/<start_d>/<end_d>', methods=['GET'])
def get_profitlossoverperiod(start_d=None, end_d=None):
    profitloss = dao.profit_loss_query(start_d, end_d)
    dao.db.commit()

    instruments = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                   "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"]

    dict_profitloss = []
    for x in range(len(instruments)):
        tempDict = {}
        tempDict["instrumentName"]=instruments[x]
        tempDict["profitLoss"] = profitloss[x+1]
        dict_profitloss.append(tempDict)

    return json.dumps(dict_profitloss), 212

@app.route('/effprofitloss', methods=['GET'])
def get_effprofitloss():
    effprofitloss = dao.effective_profit_loss_query()
    dao.db.commit()

    instruments = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                   "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"]

    dict_effprofitloss = []
    for x in range(len(instruments)):
        tempDict = {}
        tempDict["instrumentName"]=instruments[x]
        tempDict["effProfitLoss"] =  round(float(effprofitloss[x]),2)
        dict_effprofitloss.append(tempDict)

    return json.dumps(dict_effprofitloss), 213

@app.route('/realeffprofitloss', methods=['GET'])
def get_realeffprofitloss():
    profitloss = dao.profit_loss_query()
    effprofitloss = dao.effective_profit_loss_query()
    dao.db.commit()

    instruments = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                   "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"]

    dict_realeffprofitloss = []
    for x in range(len(instruments)):
        tempDict = {}
        tempDict["instrumentName"] = instruments[x]
        tempDict["realProfit"] = round(float(profitloss[x+1]),2)
        tempDict["effProfit"] =  round(float(effprofitloss[x]),2)
        dict_realeffprofitloss.append(tempDict)

    return json.dumps(dict_realeffprofitloss), 214

def bootapp():
    #app.run(port=app.config["PORTFLASK"], threaded=True, host=app.config["IPFLASK"])
    app.run(port="8080", threaded=True, host="0.0.0.0")


if __name__ == '__main__':
    bootapp()
