import mysql.connector
from datetime import date, timedelta, datetime

class DAO:
    def __init__(self, config):
        #self.db = mysql.connector.connect(host=config['IPDAO'],database=config['DB'],user=config['DBUSER'],password=config['DBPASS'])
        self.db = mysql.connector.connect(host='192.168.99.100', database='trade_history', user='root',
                                          password='ppp')
        self.dealdb = self.db.cursor(prepared=True)


    def month_to_int(self, name):
        if name == "Jan": return "01"
        elif name == "Feb": return "02"
        elif name == "Mar": return "03"
        elif name == "Apr": return "04"
        elif name == "May": return "05"
        elif name == "Jun": return "06"
        elif name == "Jul": return "07"
        elif name == "Aug": return "08"
        elif name == "Sep": return "09"
        elif name == "Oct": return "10"
        elif name == "Nov": return "11"
        elif name == "Dec": return "12"
        else: raise ValueError

    # Data from the data generator

    def split_date_time(self, deal):

        temp_date = deal['time']

        # separate time and date
        deal['time'] = temp_date[13:21]

        # update date
        deal['date'] = temp_date[7:11] + "-" + self.month_to_int(temp_date[3:6]) + "-" + temp_date[0:2]

        return deal

    # Write into the Database
    def write_into_db(self, deal):

        deal = self.split_date_time(deal)

        self.dealdb.execute("SELECT instrument_id FROM instrument WHERE instrument_name = %s", (deal['instrumentName'], ))
        instrumentID = self.dealdb.fetchall()
        print("InsturmentID", instrumentID)
        if instrumentID != []:
            instrumentID = instrumentID[0][0]

        self.dealdb.execute("SELECT counterparty_id FROM counterparty WHERE counterparty_name = %s",
                            (deal['cpty'],))
        ctpyID = self.dealdb.fetchall()
        print("Ctpy ID", ctpyID)
        if ctpyID != []:
            ctpyID = ctpyID[0][0]

        if ctpyID != [] and instrumentID != []:
            # write into deal table
            deal_statement = "INSERT INTO deal (price, instrument_id, counterparty_id, deal_type, quantity, deal_date, deal_time) VALUES (%s, %s, %s, %s, %s, %s, %s);"
            self.dealdb.execute(deal_statement, (deal['price'], instrumentID, ctpyID, deal['type'], deal['quantity'], deal['date'], deal['time'], ))
        else:
            print("Insert not possible.")

    # Retrieve data from Database

    def retrieve_chart_from_db(self):        # print all deal data
        selectQuery = "SELECT instrument_name, price, deal_type, deal_date, deal_time FROM deal join instrument on (deal.instrument_id = instrument.instrument_id)"
        self.dealdb.execute(selectQuery)

        # print all entries in deal table
        records = self.dealdb.fetchall()
        print('Deal data retrieved.')

        return records

    def retrieve_ctpy_from_db(self):
        selectQuery = "SELECT counterparty_name, COUNT(*) FROM deal join counterparty on (deal.counterparty_id = counterparty.counterparty_id) GROUP BY counterparty_name"
        self.dealdb.execute(selectQuery)

        # print all entries in deal table
        records = self.dealdb.fetchall()
        print(records)

        return records

    def retrieve_instrument_from_db(self):  # print all instrument data
        selectQuery = "SELECT * FROM instrument"
        self.dealdb.execute(selectQuery)

        # print all entries in instrument table
        records = self.dealdb.fetchall()
        print('Instrument data retrieved.')

        return records

    def retrieve_price_from_db(self):   # print price from deal
        selectQuery = "SELECT price FROM deal"
        self.dealdb.execute(selectQuery)

        # print all prices
        records = self.dealdb.fetchall()
        print('Price data retrieved.')

        return records

    def retrieve_deal_over_date_period(self, start_d, end_d=None):
        if end_d == None:
            end_d = date.today() - timedelta(days = 1)

        selectQuery = self.chart_select_method()
        selectQuery = selectQuery + "WHERE deal_date >= %s AND deal_date <= %s"
        print(selectQuery)

        self.dealdb.execute(selectQuery, (start_d, end_d))
        records = self.dealdb.fetchall()
        print(records)
        print('Deal data over date period retrieved.')

        return records

    def retrieve_deal_over_time_period(self, start_t=None, end_t=None, date_choice=None):
        if start_t == None:
            start_t = "00:00:00"
        if date_choice == None:
            date_choice = date.today() - timedelta(days = 1)
        if end_t == None:
            end_t = datetime.now().time()

        selectQuery = self.chart_select_method()
        selectQuery = selectQuery + "WHERE deal_date = %s AND deal_time >= %s AND deal_time <= %s"
        print(selectQuery)

        print(selectQuery, (date_choice, start_t, end_t))
        self.dealdb.execute(selectQuery, (date_choice, start_t, end_t))

        records = self.dealdb.fetchall()
        print(records)

        return records

    def retrieve_deal_at_date_over_time_period(self, date_choice, start_t=None, end_t=None):
        if start_t == None:
            start_t = "00:00:00"
        if end_t == None:
            end_t = datetime.now().time()

        selectQuery = self.chart_select_method()
        selectQuery = selectQuery + "WHERE deal_date = %s AND deal_time >= %s AND deal_time <= %s"

        self.dealdb.execute(selectQuery, (date_choice, start_t, end_t))
        print(selectQuery, (date_choice, start_t, end_t))

        records = self.dealdb.fetchall()
        print('Deal data on date over time period retrieved.')

        return records

    def chart_select_method(self):
        return "SELECT instrument_name, price, deal_type, deal_date, deal_time FROM deal JOIN instrument on (deal.instrument_id = instrument.instrument_id) "

    def ctpy_select_method(self):
        return "SELECT counterparty_name, COUNT(*) FROM deal join counterparty on (deal.counterparty_id = counterparty.counterparty_id) GROUP BY counterparty_name"

    def end_positions_query(self):
        records = []
        for x in range(1,13,1):
            selectBQuery = "SELECT SUM(quantity) AS bought INTO @qty_bought from deal WHERE deal_type = \"B\" AND instrument_id = %s"
            selectSQuery = "SELECT SUM(quantity) AS sold INTO @qty_sold from deal WHERE deal_type = \"S\" AND instrument_id = %s"
            self.dealdb.execute(selectBQuery, (str(x), ))
            self.dealdb.execute(selectSQuery, (str(x), ))
            self.dealdb.execute("SELECT @qty_bought - @qty_sold;")

            # print all entries in deal table
            records.append(self.dealdb.fetchall())
        return records

    def profit_loss_query(self, start_d=None, end_d=None):
        if start_d == None and end_d == None:
            selectQuery = "SELECT instrument_id, quantity, price, deal_type FROM deal"
            self.dealdb.execute(selectQuery)
        else:
            selectQuery = "SELECT instrument_id, quantity, price, deal_type FROM deal WHERE deal_date >= %s AND deal_date <= %s"
            self.dealdb.execute(selectQuery, (start_d, end_d, ))
            print(selectQuery, (start_d, end_d, ))

        records = []
        records = self.dealdb.fetchall()

        instrumentIDs = list(range(1,13))
        result_dict = {}
        for ID in instrumentIDs:
            result_dict[ID] = 0

        for row in records:
            if row[3] == 'B':
                temp_price = -1 * row[2]
            else:
                temp_price = row[2]
            result_dict[row[0]] += temp_price*row[1]

        return result_dict

    def effective_profit_loss_query(self):
        abs_profitloss = self.profit_loss_query()
        print(abs_profitloss)

        selectBSQuery = "SELECT quantity, instrument_id, deal_type FROM deal"

        self.dealdb.execute(selectBSQuery)
        records = []
        records = self.dealdb.fetchall()

        selectPriceQuery = "SELECT price from deal"
        self.dealdb.execute(selectPriceQuery)
        recordP = []
        recordP = self.dealdb.fetchall()

        currentPrice = recordP[-1][0]

        instrumentIDs = list(range(1, 13))
        result_dict = {}
        for ID in instrumentIDs:
            result_dict[ID] = 0

        for row in records:
            if row[2] == 'B':
                temp_quantity = -1 * row[0]
            else:
                temp_quantity = row[0]
            result_dict[row[1]] += temp_quantity*currentPrice

        returnList =[]
        for x,y in zip(abs_profitloss.values(),result_dict.values()):
                returnList.append(x+y)

        return returnList






