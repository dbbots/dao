import time
import numpy, random
from datetime import datetime, timedelta
import json

class Instrument:
    def __init__(self,base_price,instr_id,variance,drift,instrument_name):
        self.__startingPrice = base_price
        self.__price = base_price
        self.__id = instr_id
        self.__variance = variance
        self.__drift = drift
        self.name = instrument_name

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0, 1) * self.__variance + self.__drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < self.__startingPrice * 0.4:
            self.__drift = (-0.7 * self.__drift)
        self.__price = newPrice * 1.01 if direction == 'B' else newPrice * 0.99
        return self.__price







