FROM python:3

# DAO
FROM ubuntu:latest
MAINTAINER BOTS
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN pip install mysql-connector-python
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["./app.py"]

# Mysql
RUN apt-get install mysql-server -y
