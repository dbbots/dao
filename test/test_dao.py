import json

from dao import DAO
from dao_flask import configObj
import unittest
class DaoTest(unittest.TestCase):

    def setUp(self):
        self.dao = DAO(configObj)
        self.deal = {'instrumentName': 'Koronis', 'cpty': 'Lina', 'price': 2439.608282220985, 'type': 'S', 'quantity': 99, 'time': '13-Aug-2019 (12:09:46.202198)'}


    def test_month_to_int(self):
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
        num_months = ['01','02','03','04','05','06','07','08','09','10','11','12']

        for month in range(len(months)):
            result = self.dao.month_to_int(months[month])

            self.assertEqual(num_months[month], result, msg="Month number is not as expected.")

    def test_fail_month_to_int(self):
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Mal']
        num_months = ['01','02','03','04','05','06','07','08','09','10','11','12']
        raised = False
        for month in range(len(months)):
            raised = False
            try:
                self.dao.month_to_int(months[month])
            except:
                raised = True

        self.assertTrue(raised, 'Correct month found.')

    def test_write_into_db(self):
        raised = True
        try:
            self.dao.write_into_db()
        except:
            raised = False
        self.assertFalse(raised, 'Write test failed.')

    def test_retrieve_deal_from_db(self):
        raised = False
        try:
            self.dao.retrieve_chart_from_db()
        except:
            raised = True
        self.assertFalse(raised, 'Retrieve deal table from DB failed.')

    def test_retrieve_instrument_from_db(self):
        raised = False
        try:
            self.dao.retrieve_instrument_from_db()
        except:
            raised = True
        self.assertFalse(raised, 'Retrieve instrument table from DB failed.')

    def test_retrieve_price_from_db(self):
        raised = False
        try:
            self.dao.retrieve_price_from_db()
        except:
            raised = True
        self.assertFalse(raised, 'Retrieve price from DB failed.')

    def test_retrieve_deal_over_date_period(self):
        raised = False
        start_d = "1990-01-01"
        end_d = "2222-01-01"
        try:
            self.dao.retrieve_deal_over_date_period(start_d, end_d)
        except:
            raised = True
        self.assertFalse(raised, 'Retrieve deals between date period failed.')

    def test_retrieve_deal_over_time_period(self):
        raised = False
        start_t = "00:00:01"
        end_t = "23:59:59"
        try:
            self.dao.retrieve_deal_over_time_period(start_t, end_t)
        except:
            raised = True
        self.assertFalse(raised, 'Retrieve deals between date period failed.')

    def test_retrieve_deal_at_date_over_time_period(self):
        raised = False
        date_choice = "2019-08-12"
        start_t = "00:00:01"
        end_t = "23:59:59"
        try:
            self.dao.retrieve_deal_at_date_over_time_period(date_choice, start_t, end_t)
        except:
            raised = True
        self.assertFalse(raised, 'Retrieve deals between date period failed.')